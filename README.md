# Cl4p-i
Cl4p-i is a robot running under Circuit and assisting the team. It provides several shortcuts and functionalities useful both for meetings and workflow management.

### Calling Cl4p-i
Cl4p-i is considered by Circuit as a user. To talk to him, you can either send him a message directly or add him in a conversation with his circuit name. 
You should have created one robot on your instance and get his client ID and his client secret key.