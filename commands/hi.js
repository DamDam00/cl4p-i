const db = require('../data/database')
const tools = require('../tools/tools')
module.exports = async (client, convId, userReference, args, itemId, message) => {
    client.getUserById(userReference).then(user => {
        db.find({ _id: 'hiSalutations' }, function (err, docs) {
            let hi = tools.randomIndex(docs[0].content)
            db.find({ _id: 'hiQuestions' }, function (err, docs) {
                let question = tools.randomIndex(docs[0].content)
                client.addTextItem(convId, tools.formatReply(itemId, `${hi} ${user.firstName}<br>${question}`))
            })
        })
    })
}

var config = module.exports.config = {
    name: "Hello",
    alias: "hi",
    description: "La politesse",
    commands: ["pas d'arguments"],
    usage: ['commands | alias']
}