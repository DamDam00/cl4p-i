const tools = require('../tools/tools')
const fs = require('fs')
//object is evt or itemAdded informations from the eventListener
module.exports = async (client, convId, userReference, args, itemId, message) => {
    var list = ['<b>Commandes : </b>'];
    for (let [k, v] of client.commands) {
        list.push(` - ${v.config.alias}`)
    }

    client.addTextItem(convId, tools.formatReply(itemId, list.join('<br>')))
}

var config = module.exports.config = {
    name: "help",
    alias: "help",
    description: "Commandes d'aides.",
    commands: ["pas d'arguments"],
    usage: ['commands | alias']
}