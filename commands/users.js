const tools = require('../tools/tools.js')
const db = require('../data/database')

module.exports = (client, convId, userReference, args, itemId, message) => {
    if (args[0] == 'add') {
        if (permissions.checkUserPermissions(client, userReference, convId, itemId) == true) {
            const getUserByEmail = client.getUserByEmail(args[1]);
            getUserByEmail.then(user => {
                db.update({ _id: "usersAutorised" }, { $addToSet: { 'listUsersAutorised': user.userId } })
                client.addTextItem(convId, tools.formatReply(itemId, `Utilisateur ${user.displayName} ajouté aux utilisateurs autorisés`))
            })
                .catch(error => client.addTextItem(convId, tools.formatReply(itemId, `Pas d'utilisateur connu avec cette adresse mail.`))
                )
        }
        else
            client.addTextItem(convId, tools.formatReply(itemId, `Vous ne disposez pas des droits pour accéder à cette ressource`))
                .catch(error => console.log(error))
    }

    if (args[0] == 'remove') {
        if (args[0] == 'add') {
            if (permissions.checkUserPermissions(client, userReference, convId, itemId) == true) {
                const getUserByEmail = client.getUserByEmail(args[1]);
                getUserByEmail.then(user => {
                    db.update({ _id: "usersAutorised" }, { $pull: { 'listUsersAutorised': user.userId } })
                    client.addTextItem(convId, tools.formatReply(itemId, `Utilisateur ${user.displayName} retiré des utilisateurs autorisés`))
                })
                    .catch(error => client.addTextItem(convId, tools.formatReply(itemId, `Pas d'utilisateur connu avec cette adresse mail.`))
                    )

            }
        }
    }

    if (args[0] == 'list') {
        let responseArray = new Array();
        db.find({ _id: "usersAutorised" }, function (err, docs) {
            docs[0].listUsersAutorised.forEach(element => {
                client.getUserById(element)
                    .then(user => {
                        console.log('loop ' + test)
                        return responseArray.push(user)
                    })
                    .catch(error => console.log(error))
            });
            client.addTextItem(convId, tools.formatReply(itemId, test))
        })

    }

    else {
        client.addTextItem(convId, tools.formatReply(itemId,
            `Description : ${config.description}<br>Commandes : ${config.commands}`))
            .catch(err => console.error(err))
    }
}

var config = module.exports.config = {
    name: "Users",
    alias: "users",
    description: "Gestion des droits utilisateurs.",
    commands: ["add", "remove", "list"],
    usage: ['commands | alias']
}

