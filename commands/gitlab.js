const gitlabApi = require('@gitbeaker/node')
const tools = require('../tools/tools.js')
const db = require('../data/database')
const permissions = require('../data/checkPermissions')

const api = new gitlabApi.Gitlab({
    host: process.env.GITLAB_URL,
    token: process.env.GITLAB_API,
})

module.exports = async (client, convId, userReference, args, itemId, message) => {
    if (permissions.checkUserPermissions(client, userReference, convId, itemId) == true) {
        if (args[0] == 'commits') {
            await api.Commits.all(args['1'], { maxPages: 1, perPage: 40 })
                .then(resolve => {
                    var array = [];
                    for (let index = 0; index < resolve.length; index++) {
                        array[index] = '- ' + resolve[index].title + '<br>'
                    }
                    array.unshift('<b>Liste des commits pour le projet : ' + args[1] + ' </b><br>')
                    client.addTextItem(convId, tools.formatReply(itemId, array.join('-')))
                        .catch(error => console.log(error))
                });
        }

        else {
            client.addTextItem(convId, tools.formatReply(itemId,
                `Description : ${config.description}<br>Commandes : ${config.commands}`))
                .catch(err => console.error(err))
        }
    }

    else {
        client.addTextItem(convId, tools.formatReply(itemId, `Vous n'avez pas les droits nécessaires pour cette commandes`))
    }
}

var config = module.exports.config = {
    name: "Gitlab",
    alias: "gitlab",
    description: "Commandes permettant de manager le Gitlab de l'équipe à distance",
    commands: ['commits'],
    usage: ['commands | alias']
}