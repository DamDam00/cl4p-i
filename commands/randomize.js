const tools = require('../tools/tools.js')
const db = require('../data/database')
module.exports = async (client, convId, userReference, args, itemId, message) => {
    if (args[0] == "init") {
        client.getConversationById(convId)
            .then(function (conversation) {
                var arrayRandomized = tools.shuffle(conversation.participants)
                // Using a promise to resolve and return the datas (the sentItem read only promise. Can be enhanced for the futur)
                client.getUsersById(arrayRandomized)
                    .then(
                        users => {
                            // Performance issue ? Duplicate datas (if not -> initialization error)
                            var arrayResponse = users
                            for (let index = 0; index < users.length; index++) {
                                //Check and delete if the botId is found
                                if (users[index].userId != global.gConfig._botId && users[index].userState == "ACTIVE")
                                    arrayResponse[index] = '<br> ' + (index + 1) + ' - ' + users[index].displayName;

                                else {
                                    arrayResponse.splice(index, 1)
                                    index--;
                                }
                            }
                            db.remove({ 'randomConvid': convId }, {}, function () { })
                            db.insert({ 'randomConvid': convId, 'usersRandomize': arrayResponse }, function (err, newDocs) {
                                client.addTextItem(convId, tools.formatReply(itemId, 'Tirages au sort aléatoire des participants : <br> ' +
                                    arrayResponse.join())).then(resolve => {
                                        client.addTextItem(convId, tools.formatReply(itemId, `Premier tiré au sort : @${arrayResponse[0]}`))
                                    }).then(resolve => {
                                        db.update({ 'randomConvid': convId }, { $pop: { usersRandomize: -1 } }, function () { })
                                    })
                                    .catch(error => console.log(error))
                            })
                        })
            })
    }

    if (args[0] == "next") {
        db.find({ 'randomConvid': convId }, function (err, docs) {
            if (docs[0].usersRandomize.length == 0)
                client.addTextItem(convId, tools.formatReply(itemId, `Tirage terminé`))
            else {
                client.addTextItem(convId, tools.formatReply(itemId, `Tirage : ${docs[0].usersRandomize[0]}`))
                db.update({ 'randomConvid': convId }, { $pop: { usersRandomize: -1 } }, function () { })
            }
        })
    }

    else {
        client.addTextItem(convId, tools.formatReply(itemId,
            `Description : ${config.description}<br>Commandes : ${config.commands}`))
            .catch(err => console.error(err))
    }
}

var config = module.exports.config = {
    name: "Randomize",
    alias: "randomize",
    description: "Retourne une liste de participants dans un ordre aléatoire",
    commands: ['init', 'next'],
    usage: ['commands | alias']
}