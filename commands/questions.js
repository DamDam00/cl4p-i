const tools = require('../tools/tools')
var db = require('../data/database')

/**
 * convId est l'ID unique de la conversation en cours. Il est récupéré grâce à l'event mention
 * Il sert de clé primaire dans la mini bdd intégrée.
 */
//(client, conversation, evt, args, itemId)
module.exports = async (client, convId, userReference, args, itemId, message) => {
    if (args[0] == 'start') {
        db.find({ 'questions-convId': convId }, function (err, docs) {
            if (docs.length == 0) {
                client.addTextItem(convId, tools.formatReply(itemId, "Démarrage d'une session de questions/réponses"))
                db.insert({ 'questions-convId': convId, 'questions': [] }, function (err, newDocs) { })
            }
            else
                client.addTextItem(convId, tools.formatReply(itemId, "Une session est déjà en cours"))
        })

    }

    if (args[0] == 'end') {
        db.find({ 'questions-convId': convId }, function (err, docs) {
            if (docs.length == 1) {
                db.remove({ 'questions-convId': convId }, function (err, docs) {
                    client.addTextItem(convId, tools.formatReply(itemId, "Fin de la session de questions/réponses"))
                })
            }
            else
                client.addTextItem(convId, tools.formatReply(itemId, "Aucune session n'est démarrée"))
        })
    }

    if (args[0] == 'add') {
        db.find({ 'questions-convId': convId }, function (err, docs) {
            if (docs.length == 1) {
                var question = args.splice(1, args.length).join(' ');
                //var session = await keyv.get(convId, question)
                //await keyv.set(convId, question)
                client.getUserById(userReference)
                    .then(user => {
                        db.update({ 'questions-convId': convId }, { $push: { questions: `${question} par <b>${user.displayName}</b>` } }, {}, function () {
                            client.addTextItem(convId, tools.formatReply(itemId, "Ajout de la question à la liste : <br> -" + question))
                        })
                    })
            }
            else
                client.addTextItem(convId, tools.formatReply(itemId, "Aucune session n'est démarrée"))
        })
    }

    if (args[0] == 'showall' || args[0] == 'list') {
        db.find({ 'questions-convId': convId }, function (err, docs) {
            if (docs.length == 1) {
                client.addTextItem(convId, tools.formatReply(itemId,
                    '<b> Liste des questions en attentes : </b></br> ' + docs[0].questions.join('<br>')))
            }
        })
    }

    if (args[0] == 'next') {
        db.find({ 'questions-convId': convId }, function (err, docs) {
            if (docs[0].questions.length != 0) {
                db.update({ 'questions-convId': convId }, { $pop: { questions: -1 } }, {}, function () {
                    client.addTextItem(convId, tools.formatReply(itemId,
                        '<b> Question en cours : </b> </br> ' + docs[0].questions[0]))
                })
            }
            else {
                client.addTextItem(convId, tools.formatReply(itemId, `<b>Il n'y a plus de questions</b>`))
            }
        })
    }

    else {
        client.addTextItem(convId, tools.formatReply(itemId,
            `Description : ${config.description}<br>Commandes : ${config.commands}`))
            .catch(err => console.error(err))
    }
}


var config = module.exports.config = {
    name: "Questions",
    alias: "questions",
    description: "Permet de lancer une session de questions / réponses. Chaque particpant peut ajouter une question à la liste",
    commands: ['start', 'add', "next", "showall", "end"],
    usage: ['commands | alias']
}