//***********************************************************
// * Launch express server to receive webhook from Gitlab 
//***********************************************************

// Require express and body-parser
const Circuit = require('circuit-sdk')
const fs = require('fs')
const config = require('./config')
const url = require('url');
require('dotenv').config()

if (process.env.PROXY != 'noproxy') {
  const HttpsProxyAgent = require('https-proxy-agent');
  Circuit.NodeSDK.proxyAgent = new HttpsProxyAgent(url.parse(process.env.PROXY));
}

//***********************************************************
// * Circuit login
//***********************************************************
const client = new Circuit.Client({
  client_id: process.env.CLIENT_ID,
  client_secret: process.env.CLIENT_SECRET,
  domain: process.env.DOMAIN,
  removeMentionHtml: true,
  scope: global.gConfig._scope
});
client.commands = new Map();

//Add commands to client object
//Read all files and load them in require
fs.readdir('./commands', (err, files) => {
  if (err)
    return console.error(err)
  files.forEach(file => {
    if (!file.endsWith('.js'))
      return undefined
    const props = require(`./commands/${file}`);
    const cmdName = file.split('.')[0];
    client.commands.set(cmdName, props)
  });
})

//Add event to client object
fs.readdir('./events', (err, files) => {
  if (err)
    return console.error(err)
  files.forEach(file => {
    if (!file.endsWith('.js'))
      return undefined
    const evtName = file.split('.')[0];
    client.addEventListener(evtName, function (evt) {
      require(`./events/${file}`)(client, evt)
    })
  })
})


client.logon()
  .then(function (user) {
    console.log('Successfully authenticated as ' + user.displayName)
    client.setPresence({ state: Circuit.Enums.PresenceState.AVAILABLE })
    global.gConfig._botId = user.userId
  }).catch(error =>
    console.log('erreur during logon : ' + error))




