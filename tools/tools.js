const Circuit = require('circuit-sdk')

module.exports = {

    /**
     * @returns RICH type message
     * @param {id} _parentId 
     * @param {String} _message 
     * @description _parentId is used to made a reply and not a new topic with 
     *              large whitespace in conversation. 
     *              Little enhancement for better UI. 
     *              /!\ topLevelItem can have object "itemId" (bot call from a reply) or
     *              "parentItemId" (bot call from a (new) topic). check it before to avoid 
     *              the "retention data" error
     */
    formatReply: function (_parentId, _message) {
        return content = {
            parentId: _parentId,
            content: _message,
            contentType: Circuit.Enums.TextItemContentType.RICH
        }
    },
    shuffle: function (array) {
        /* Randomize array in-place using Durstenfeld shuffle algorithm */
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    },

    randomIndex: function(array) {
        let test = array[Math.floor(Math.random() * array.length)]
        return test;
    }
}