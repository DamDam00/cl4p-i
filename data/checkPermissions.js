const db = require('./database')
const tools = require ('../tools/tools')
module.exports = {

    checkUserPermissions: function (client, userReference, convId, itemId) {
        db.find({ "listUsersAutorised": userReference }, async function (err, docs) {
            if (docs.length == 1)
                return true
            else {
                client.addTextItem(convId, tools.formatReply(itemId, `Vous ne disposez pas des droits pour accéder à cette ressource`))
                    .catch(error => console.log(error))
                return false
            }
        })
    }
}