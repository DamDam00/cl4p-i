var Datastore = require('nedb')
const fs = require('fs');
const dbPath = './data/db.json'
//if file exist, load the file, if not, load the config files
try {
    if (fs.existsSync(dbPath)) {
        db = new Datastore({ filename: dbPath, autoload: true });
    }

    if (!fs.existsSync(dbPath)) {
        db = new Datastore({ filename: dbPath, autoload: true });
        //Load responses JSON 
        //Load your JSON, here I'm just hardcoding it
        var listResponses = fs.readFileSync('./data/responses.json')
        var myData = JSON.parse(listResponses)
        // Insert into your database

        db.insert(myData, function (err) {
            if (err)
                console.log('Error during database loading : ' + err)
        })

        var members = fs.readFileSync('./data/iotsec.json')
        var myData = JSON.parse(members)

        //Load Id of iotsec members
        db.insert(myData, function (err) {
            if (err)
                console.log('Error during database loading : ' + err)
        })
    }
} catch (err) {
    console.log(err)
}
module.exports = db