FROM node:12-alpine

RUN apk add --no-cache --virtual .gyp \
        python \
        make \
        g++

# Create app directory
WORKDIR /usr/src/app

# Environnement variables
ENV GITLAB_URL=gitlab_url \ 
    GITLAB_API=gitlab_api \
    CLIENT_ID=client_id \
    CLIENT_SECRET=client_secret \
    DOMAIN=domain \
    PROXY=noproxy


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Copy npm config
COPY .npmrc /root/.npmrc

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 3000

CMD [ "node", "index.js" ]