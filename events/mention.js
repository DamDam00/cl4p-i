const Circuit = require('circuit-sdk');
const tools = require('../tools/tools');
const _ = require('lodash');
//***********************************************************
// * Bot listen to mention him
//***********************************************************

/**
 * client is the circuit client containing bot token and other to interact with Circuit
 * conversation contain conversation infos (participants, date, itemId, userId)
 * evt contain event infos
 * args are the arguments after the first command beginning with "!"
 * itemId is use to determine if this is a new topic or a reply (avoid new topic for each response)
 */
module.exports = (client, evt) => {

    client.getConversationById(evt.mention.itemReference.convId)
        .then(function (conversation) {
            //Check format
            if (conversation.topLevelItem.text.state == "CLUSTERED") {
                messages = conversation.topLevelItem.text.content.split("<hr>")
                conversation.topLevelItem.text.content = messages[messages.length - 1]
            }
            if (formatMessageMention(conversation.topLevelItem.text.content)) {
                var itemId = (conversation.topLevelItem.parentItemId === undefined) ? conversation.topLevelItem.itemId : conversation.topLevelItem.parentItemId;
                args = (conversation.topLevelItem.text.content.slice(conversation.topLevelItem.text.content.indexOf(`@${global.gConfig._botName}`)).trim().split(/ +/g));
                args.shift();
                if (args[0].charAt(0) == global.gConfig._prefix) {
                    command = args.shift().substring(1).toLowerCase();
                    if (client.commands.has(command))
                        client.commands.get(command)(client, evt.mention.itemReference.convId, evt.mention.userReference.userId, args, itemId, evt)
                            .catch(err => console.error(err))
                    else {
                        client.addTextItem(evt.mention.itemReference.convId, tools.formatReply(itemId, `Je n'ai pas compris votre commande. Consultez l'aide en tapant '!help'.`))
                    }
                }
                else
                    client.addTextItem(message.item.convId, tools.formatReply(message.item.itemId, "Veuillez ajouter un '!' devant votre commande"))
                        .catch(error => console.log(error))
            }
        })
}

/**
 * @description check the format message (@botName !cmd args)
 * @param {String} message 
 */
function formatMessageMention(message) {

    if (message.substring(0, global.gConfig._botName.length + 1) == `@${global.gConfig._botName}`)
        return true
    else
        return false
}