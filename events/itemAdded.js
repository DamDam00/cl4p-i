const Circuit = require('circuit-sdk');
const tools = require('../tools/tools');

module.exports = (client, message) => {


    if (!mention(message)) {
        if (!sentByMe(message.item.creatorId)) {
            client.getConversationParticipants(message.item.convId).then(resolve => {
                if (resolve.participants.length == 2) {

                    if (message.item.text.content.charAt(0) === '!') {
                        if (message.item.text.state == "CLUSTERED") {
                            messages = message.item.text.content.split("<hr>")
                            message.item.text.content = messages[messages.length - 1]
                        }
                        //itemId for reply or new topic (same as Mention)
                        var itemId = (message.item.itemId === undefined) ? message.item.itemId : message.item.parentItemId;
                        //add space at the end for parsing command (if not, a void string is send)
                        args = message.item.text.content.trim().split(/ +/g);
                        command = args.shift().substring(1).toLowerCase();
                        if (client.commands.has(command))
                            client.commands.get(command)(client, message.item.convId, message.item.creatorId, args, itemId, message)
                        else
                            client.addTextItem(message.item.convId, tools.formatReply(itemId, `Je n'ai pas compris votre commande. Consultez l'aide en tapant '!help'.`))
                                .catch(error => console.log(error))
                    }
                    else
                        client.addTextItem(message.item.convId, tools.formatReply(itemId, "Veuillez ajouter un '!' devant votre commande"))
                            .catch(error => console.log(error))
                }
            })
        }
    }
}

//*********************************************************************
//* Mention - check if it mention type
//*********************************************************************
function mention(item) {
    if (item.item.text.content.includes(`@${gConfig._botName}`))
        return true
    else
        return false
}

//*********************************************************************
//* sentByMe - check if the received circuit event was sent by the bot himself
//*********************************************************************
function sentByMe(item) {
    return (global.gConfig._botId === item);
};